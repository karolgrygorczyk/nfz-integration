<?php

namespace App\Tests;

use App\Integration\Nfz\Exception\CommunicationException;
use App\Integration\Nfz\FakeClient;
use App\Integration\Nfz\Model\DocumentAccess;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class IntegrationNfzFakeClientTest extends KernelTestCase
{
    public function testSomething(): void
    {
        $kernel = self::bootKernel();
        $container = $kernel->getContainer()->get('test.service_container');
        /** @var FakeClient $client */
        /** @noinspection PhpUnhandledExceptionInspection */
        $client = $container->get(FakeClient::class);

        $validAccess = [
            DocumentAccess::create('1-21-000000000-1', '70010101234'),
            DocumentAccess::create('1-21-000000000-2', '80010101234'),
            DocumentAccess::create('1-21-000000000-3', '90010101234'),

        ];
        $invalidAccess = [
            DocumentAccess::create('1-21-000000000-1', ''),
            DocumentAccess::create('', '70010101234'),
            DocumentAccess::create('1-21-000000000-2', '70010101234'),
        ];
        $communicationExceptionAccess = [
            DocumentAccess::create('1-21-000000000-4', ''),
            DocumentAccess::create('1-21-000000000-5', ''),
            DocumentAccess::create('1-21-000000000-6', ''),
        ];



        array_map(static function(DocumentAccess $access) use ($client) {
            self::assertNotNull($client->getDocument($access));
            self::assertTrue($client->documentExists($access));
        }, $validAccess);

        array_map(static function(DocumentAccess $access) use ($client) {
            self::assertNull($client->getDocument($access));
            self::assertFalse($client->documentExists($access));
        }, $invalidAccess);

        $this->expectException(CommunicationException::class);
        array_map(static function(DocumentAccess $access) use ($client) {
            $client->documentExists($access);
            $client->getDocument($access);
        }, $communicationExceptionAccess);

    }
}
