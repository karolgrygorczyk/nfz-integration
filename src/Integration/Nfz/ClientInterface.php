<?php

declare(strict_types=1);

namespace App\Integration\Nfz;

use App\Integration\Nfz\Model\Document;
use App\Integration\Nfz\Model\DocumentAccess;

interface ClientInterface
{
    public function documentExists(DocumentAccess $access): bool;
    public function getDocument(DocumentAccess $access): ?Document;
}
