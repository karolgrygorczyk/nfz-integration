<?php

namespace App\Integration\Nfz;

use App\Integration\Nfz\Model\Document;
use App\Integration\Nfz\Model\DocumentAccess;
use App\Integration\Nfz\Model\MedicalComponent;

class FakeClient implements ClientInterface
{
    private $fakeData;

    public function __construct()
    {
        $this->fakeData = [
            '1-21-000000000-1' => [
                'pid' => '70010101234',
                'document' => new Document(
                    '2021-05',
                    3,
                    2,
                    new MedicalComponent(
                        'P.137',
                        'P.137',
                        'Czujnik do systemu monitorowania stężenia glukozy Flash (Flash Glucose Monitoring - FGM) do 2 sztuk'
                    )
                )
            ],
            '1-21-000000000-2' => [
                'pid' => '80010101234',
                'document' => new Document(
                    '2021-01',
                    12,
                    4,
                    new MedicalComponent(
                        'P.135',
                        'P.135',
                        'Sensor/ Elektroda do Systemu Ciągłego Monitorowania Glikemii w czasie rzeczywistym (CGM-RT) do 4 sztuk'
                    )
                )
            ],
            '1-21-000000000-3' => [
                'pid' => '90010101234',
                'document' => new Document(
                    '2021-02',
                    12,
                    5,
                    new MedicalComponent(
                        'P.134',
                        'P.134',
                        'Zbiornik na insulinę do osobistej pompy insulinowej do 5 sztuk'
                    )
                )
            ]
        ];
    }

    public function documentExists(DocumentAccess $access): bool
    {
        $document = $this->getDocument($access);
        return null !== $document;
    }

    public function getDocument(DocumentAccess $access): ?Document
    {
        $data = $this->fakeData[$access->getDocumentNumber()] ?? null;
        return null !== $data && $data['pid'] === $access->getPersonIdentificationNumber()
            ? $data['document']
            : null;
    }
}
