<?php

declare(strict_types=1);

namespace App\Integration\Nfz\Model;

final class DocumentAccess
{
    /**
     * Numer dokumentu zlecenia
     * @var string
     */
    private $documentNumber;
    /**
     * Pesel
     * @var string
     */
    private $personIdentificationNumber;

    public function __construct(string $documentNumber, string $personIdentificationNumber)
    {
        $this->documentNumber = $documentNumber;
        $this->personIdentificationNumber = $personIdentificationNumber;
    }

    public static function create(string $documentNumber, string $personIdentificationNumber): self
    {
        return new self($documentNumber, $personIdentificationNumber);
    }

    public function getDocumentNumber(): string
    {
        return $this->documentNumber;
    }

    public function getPersonIdentificationNumber(): string
    {
        return $this->personIdentificationNumber;
    }

}
