<?php

declare(strict_types=1);

namespace App\Integration\Nfz\Model;

final class Document
{
    /**
     * Pierwszy realizowany miesiąc (RRRR-MM)
     * @var string
     */
    private $startMonth;
    /**
     * Liczba miesięcy które będą realizowane
     * @var int
     */
    private $monthsNumber;
    /**
     * Liczba sztuk na miesiąc
     * @var int
     */
    private $quantityPerMonth;
    /**
     * Wyrób medyczny
     * @var MedicalComponent
     */
    private $medicalComponent;

    public function __construct(string $startMonth, int $monthsNumber, int $quantityPerMonth, MedicalComponent $medicalComponent)
    {
        $this->startMonth = $startMonth;
        $this->monthsNumber = $monthsNumber;
        $this->quantityPerMonth = $quantityPerMonth;
        $this->medicalComponent = $medicalComponent;
    }

    public function getStartMonth(): string
    {
        return $this->startMonth;
    }

    public function getMonthsNumber(): int
    {
        return $this->monthsNumber;
    }

    public function getQuantityPerMonth(): int
    {
        return $this->quantityPerMonth;
    }

    public function getMedicalComponent(): MedicalComponent
    {
        return $this->medicalComponent;
    }
}
