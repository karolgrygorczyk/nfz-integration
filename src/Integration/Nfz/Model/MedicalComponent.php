<?php

declare(strict_types=1);

namespace App\Integration\Nfz\Model;

final class MedicalComponent
{
    /**
     * Kod wyrobu medycznego
     * @var string
     */
    private $code;
    /**
     * Kod grypy wyrobów medycznych
     * @var string
     */
    private $groupCode;
    /**
     * Nazwa wyrobu medycznego
     * @var string
     */
    private $name;

    public function __construct(string $code, string $groupCode, string $name)
    {
        $this->code = $code;
        $this->groupCode = $groupCode;
        $this->name = $name;
    }

    public function  getGroupCode(): string
    {
        return $this->groupCode;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
